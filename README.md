# README 

## Setup (do only once)
1. Follow the `CMSSW_11_1_7` checkout instructions from [SWGuideL1TPhase2Instructions TWiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideL1TPhase2Instructions#CMSSW_11_1_7).
2. In `CMSSW_11_1_7/src/L1Trigger/`, get this repository (or fork it and then git clone):
   ```
   git clone ssh://git@gitlab.cern.ch:7999/skkwan/phase-2-rct-3x4.git
   ```

## Instructions
1. Set up and compile
   ```
   cd plugins/	
   cmsenv
   voms-proxy-init --voms cms --valid 194:00
   USER_CXXFLAGS="-Wno-error=unused-but-set-variable -Wno-error=unused-variable" scram b -j 8
   ```
2. Make sure the input file (if local) exists
3. Run `cmsRun test_EGammaCrystalsProducer.py`

## Dataset

Search string: `dataset=/TT*/Phase2HLTTDRWinter20DIGI*/GEN-SIM-DIGI-RAW`

Picking a PU 200 dataset: `dataset=/TT_TuneCP5_14TeV-powheg-pythia8/Phase2HLTTDRWinter20DIGI-PU200_110X_mcRun4_realistic_v3-v2/GEN-SIM-DIGI-RAW`

One file (10 GB)
`xrdcp root://cmsxrootd.fnal.gov///store/mc/Phase2HLTTDRWinter20DIGI/TT_TuneCP5_14TeV-powheg-pythia8/GEN-SIM-DIGI-RAW/PU200_110X_mcRun4_realistic_v3-v2/110000/005E74D6-B50E-674E-89E6-EAA9A617B476.root .`